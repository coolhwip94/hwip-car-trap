fx_version 'cerulean'
game 'gta5'

author 'coolhwip'
description 'Car Trap by Coolhwip'
version '1.0.0'

server_scripts {
    'server/main.js',
}

client_scripts {
	'client/main.js',
}

ui_page "html/index.html"

files {
    "html/index.html",
    "html/style.css",
    "html/script.js",
}

