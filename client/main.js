// globals
let disable_attack = false
// flag used to disable attacking with UI open

// using global to avoid passing vars back and forth with ui
let closestVehicle
let closesVehiclePlate

// simple sleep function using promise
const sleep = (milliseconds) => {
    return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

const getClosestVehicleToPlayer = () => {
    const ped = PlayerPedId()
    let ped_coords = GetEntityCoords(ped)

    const closestVehicle = GetClosestVehicle(ped_coords[0], ped_coords[1], ped_coords[2], 10, 0, 70)

    return closestVehicle
}

const getEntitySpeedMPH = (entity) => {
    const speed = GetEntitySpeed(entity)
    const speedMPH = speed * 2.236936
    return speedMPH
}

// explode vehicle when it hits a certain speed
const explodeSpeedLimit = async (vehicle, speedLimit) => { 
    
    // set explosives on vehicle
    AddVehiclePhoneExplosiveDevice(closestVehicle)

    let vehicle_speed = getEntitySpeedMPH(vehicle)

    let detonated = false
    while (! detonated) {
        vehicle_speed = getEntitySpeedMPH(vehicle)

        if (vehicle_speed >= speedLimit) {
            DetonateVehiclePhoneExplosiveDevice()
            detonated = true
            ClearVehiclePhoneExplosiveDevice()
        }

        await sleep(1)
    }
}

// loop to disable player from attacking
const disableAttackLoop = async () => {
    while (disable_attack) {
        DisableControlAction(0, 24, true)
        await sleep(1)
    }
}

// open the UI
const openTrapUI = async () => {

    SendNuiMessage(JSON.stringify({type: "hwip-car-trap", display: "true", plate: closesVehiclePlate}))

    // set focus for focus and cursor
    SetNuiFocus(true, true)

    // disable attack
    DisableControlAction(0, 24, true)

    // allows player to keep moving
    SetNuiFocusKeepInput(true)

    // disallow attacking
    disable_attack = true
    disableAttackLoop()
}

// close the UI
const closeTrapUI = () => { 
    // send message to stop displaying ui
    SendNuiMessage(JSON.stringify({type: "hwip-car-trap", display: "false"}))

    // drop focus for cursor and mouse
    SetNuiFocus(false, false)

    // give input focus back to game
    SetNuiFocusKeepInput(false)

    // reset the disable_attack flag
    disable_attack = false

}

// send notification success or fail
const trapNotification = async (type = "success") => {

    SendNuiMessage(JSON.stringify({type: "hwip-car-trap", display: "false", notify: type}))

    await sleep(3000)

    SendNuiMessage(JSON.stringify({type: "hwip-car-trap", display: "false", notify: "none"}))

}

const loadAnimDict = async (animationDict) => {
    while (!HasAnimDictLoaded(animationDict)) {
        RequestAnimDict(animationDict)
        await sleep(1000)
    }
}

const loopAnimation = async (ped, animDictionary, animationName) => {

    await loadAnimDict(animDictionary)

    let duration = 5

    while (duration > 0) {
        TaskPlayAnim(ped, animDictionary, animationName, 3.0, // blend in
                3.0, // blend out
                -1, // duration, -1 for infinite
                16, // flag
                0, // playbackrate
            true, // lock x
            true, // lock y
            false // lock z
        )

        await sleep(2000)
        duration -= 2
    }

    StopAnimTask(ped, animDictionary, animationName, 1.0)
}

const trapValidation = (ped, vehicle, distanceLimit) => {

    let valid = true
    // ensure it is within distance
    const pedCoords = GetEntityCoords(ped)
    const vehCoords = GetEntityCoords(vehicle)
    const distance = GetDistanceBetweenCoords(pedCoords[0], // x1
    pedCoords[1], // y1
    pedCoords[2], // z1
    vehCoords[0], // x2
    vehCoords[1], // y2
    vehCoords[2], // z2
        false // use z coord?
    )

    if (distance > distanceLimit) {
        valid = false
    }
    return valid

}

// "https://hwip-car-trap/exit"
RegisterNuiCallbackType("exit")
// handle exit callback
on("__cfx_nui:exit", () => {
    closeTrapUI()
})

// "https://hwip-car-trap/settrap"
RegisterNuiCallbackType("settrap")
// handle exit callback
on("__cfx_nui:settrap", async (data) => { 
    
    // set the trap
    const speedLimitMPH = data.speed_limit

    // close ui
    closeTrapUI()

    // make player do animation
    const ped = PlayerPedId()
    const animDictionary = "mp_car_bomb"
    const animationName = "car_bomb_mechanic"

    // check if valid, things like distance from vehicle
    const valid = trapValidation(ped, closestVehicle, 5)
    if (valid) {
        await loopAnimation(ped, animDictionary, animationName)
        explodeSpeedLimit(closestVehicle, speedLimitMPH)
        trapNotification("success")
    } else {
        trapNotification("fail")
    }

})

const init = async () => { // get the closest vehicle to player
    closestVehicle = getClosestVehicleToPlayer()
    closesVehiclePlate = GetVehicleNumberPlateText(closestVehicle)

    // open menu
    openTrapUI()

}

// init
RegisterNetEvent("hwip-car-trap:init");
onNet("hwip-car-trap:init", () => {
    init()
})
