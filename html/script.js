const container = document.getElementsByClassName("container")
const confirmBtn = document.getElementsByClassName("confirmBtn")
const cancelBtn = document.getElementsByClassName("cancelBtn")
const detonateSpeed = document.getElementById("detonateSpeed")
const plateText = document.getElementById("plate-text")
const successMsg = document.getElementsByClassName("successMsg")
const failMsg = document.getElementsByClassName("failMsg")



// when script loads it shouldnt display
window.onload = () => {
    container[0].style.display = "none"
    successMsg[0].style.display = "none"
    failMsg[0].style.display = "none"
}

window.addEventListener("message", (event) => {
    let type = event.data.type

    if (type === "hwip-car-trap") {
        let display = event.data.display
        let notify = event.data.notify
        let plate = event.data.plate ? event.data.plate : "UNKNOWN"

        if (display === "true") {
            // display container
            plateText.textContent = plate
            container[0].style.display = ""
    
        } else if (display === "false") {
            // hide container
            container[0].style.display = "none"
        }

        // handle message
        if (notify) {

            if (notify === "success") {
                successMsg[0].style.display = ""
            } else if (notify === "fail") {
                failMsg[0].style.display = ""
            } else {
                successMsg[0].style.display = "none"
                failMsg[0].style.display = "none"
            }
        }


    }



})

confirmBtn[0].addEventListener("click", event => {
    const speed_limit = detonateSpeed.value
    // send speed limit and confirm trap setup
    fetch('https://hwip-car-trap/settrap', {method: 'POST', body: JSON.stringify({
        "speed_limit": speed_limit
    })}).catch(error => {
        // console.error(error)
        // currently receives a typeError: failed to fetch
        // doesn't cause any issues, just catching for now
    })
})

cancelBtn[0].addEventListener("click", event => {
    // call exit method
    fetch('https://hwip-car-trap/exit', {method: 'POST', body: JSON.stringify({})}).catch(error => {
        // console.error(error)
        // currently receives a typeError: failed to fetch
        // doesn't cause any issues, just catching for now
    })
})