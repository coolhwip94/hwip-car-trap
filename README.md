# Car Trap
> Car trap by Coolhwip
> This is a custom resource made for FiveM (https://docs.fivem.net/docs/)  
> There may be references to illicit items, this is for a video game.


## Usage
---
> This will simply be trigged via a command, it is up to server owners to implement their own method of initiation and checks.

- Simply type `cartrap` as a command when next to a vehicle. (don't be inside the vehicle)
![image.png](./readme_images/image.png)

- A window will appear in top right
![image-1.png](./readme_images/image-1.png)

- Enter a speed limit (MPH) and press `confirm`
> Make sure there is a license plate number showing in the popup.   
![image-2.png](./readme_images/image-2.png)

- The car will explode once the speed limit is reached.
![image-3.png](./readme_images/image-3.png)
